#!/usr/bin/python3

##################
# TODO
# check logs for old files delete errors from pcloud, if not OK, check line 209


from multiprocessing import Process, Queue
from datetime import datetime
from pcloud import PyCloud
import os, sys, time, logging, re, zipfile, subprocess
import configparser
logfilepath = "/var/log/backupCloud.log"
logging.basicConfig(filename=logfilepath, level=logging.DEBUG, format='%(asctime)s %(message)s')
logging.info("\n-=-+-=-=-+-=-=-+-=-=-+-=-=-+-=-=-+-=-\nstarting pcloud backup run\n-=-+-=-=-+-=-=-+-=-=-+-=-=-+-=-=-+-=-")

def log_global_exception(exctype, message, traceback):
	if issubclass(exctype, KeyboardInterrupt): # let keyboardInterrupt end the program
		sys.__excepthook__(exctype, message, traceback)
		return
	logging.exception("Global exception: " + str(message))

sys.excepthook = log_global_exception
config = configparser.ConfigParser()
try:
	config.read('/etc/pCloudBackupPython/settings.conf')
	localsettings = config['local']
	backupstoragepath = localsettings.get('backupstoragepath','~/saveBackup')
	backuppaths = localsettings['backuppaths'].split()
	scriptdatapath = localsettings.get('scriptdatapath','~/pCloudBackupData')
	aes256passFile = localsettings['encryptPassFile']
	pcloudsettings = config['pcloud']
	pclouduser = pcloudsettings.get('user')
	pcloudpw = pcloudsettings.get('pw')
	pcloudloc = pcloudsettings.get('location')
	pcloudroot =  pcloudsettings.get('rootdirID')
	excludeFoldersOnAllLevels = config['global']['globalExcludeDirs'].split()
except:
	logging.exception("cannot open config file /etc/pCloudBackupPython/settings.conf\nusing default settings")
	quit()

try:
	os.seteuid(1000)  # use appropriate user uid for the system: first user here
except:
	logging.info("cannot drop root privs, maybe not running as root anyway")
zipnamestoragefile=os.path.join(scriptdatapath, "zipfilenames" + datetime.today().strftime("%Y%m%d") + ".txt")
aesnamestoragefile=os.path.join(scriptdatapath, "aesfilenames" + datetime.today().strftime("%Y%m%d") + ".txt")

def createAESfile(filepath, filename):
	aesfilename = filename + ".aes256"
	aesfilepath = os.path.join(backupstoragepath, aesfilename)
	passfilepar = "file:" + aes256passFile
	sslsuccess = True
	try:
		sslproc = subprocess.run(['/usr/bin/openssl', 'aes256', '-iter', '3', '-salt', '-in', filepath, '-out', aesfilepath, '-pass', passfilepar])
		#decrypt zip.aes256 file       CHECK FOR FILE PATH for  FILE!
		#/usr/bin/openssl aes256 -iter 3 -d -salt -in $FILENAME.zip.aes256 -out ~/restoredBackup.zip -pass file:/home/sshlogin/customscripts/backup_pcloud/pCloudBackupData/backupaes256pass.txt
		#unzip thefile                CHECK FOR FILE PATH for  FILE!
		#/usr/bin/unzip -d ./restoredZIPs ./restoredBackup.zip
	except:
		logging.exception("openssl exception")
		sslsuccess = False
	return sslsuccess, aesfilename
#  RECURSIVE FUNCTION
#                      RECURSIVE FUNCTION
#  over all child directories
def filesInDir(path, queue): # recursive function on all dirs
	files = []
	dirs = []
	zipfilepref = str(os.path.abspath(path)).replace('/','').replace(" ", "").replace(".", "").replace(",", "").encode("ascii", "ignore").decode() # remove slashes , whitespaces, commas, points and only keep ascii characters
	if len(zipfilepref) > 80:
		zipfilepref = zipfilepref[13:43] + "_" + zipfilepref[len(zipfilepref)-49:]
	zipfilename = zipfilepref.strip() + ".zip"
	zipfilepath = os.path.join(backupstoragepath, zipfilename)
	if os.access(zipnamestoragefile, os.F_OK):
		fncount = 2
		filexists = True
		while filexists:
			filexists = False
			with open(zipnamestoragefile, mode='r', encoding='utf-8') as namestorage:
				for storageline in namestorage:
					if zipfilepath in storageline:
						zipfilename = zipfilepref.strip() + str(fncount) + ".zip"
						zipfilepath = os.path.join(backupstoragepath, zipfilename)
						fncount = fncount + 1
						filexists = True
						break

	with open(zipnamestoragefile, mode='a', encoding='utf-8') as namestorage:
		print(zipfilepath, file=namestorage)
	#print("path: " + path)
	try:
		with os.scandir(path) as dirIt:
			for entry in dirIt:
				if entry.is_dir(follow_symlinks=False) and (entry.name not in excludeFoldersOnAllLevels):
					childzipath, childzipname = filesInDir(entry.path, queue)
					if (childzipath != "0"): # something bad happened in child process if 0, no file returned
						if (os.stat(childzipath).st_size < 10485760): # add zip smaller than 10MB to parent's zip
							try:
								zf = zipfile.ZipFile(zipfilepath, mode='a')
							except:
								zf = zipfile.ZipFile(zipfilepath, mode='w')
							czf =  zipfile.ZipFile(childzipath, mode='r')  # add all files uncompressed to parent zip
							for fn in czf.namelist():
								zf.writestr(fn, czf.open(fn).read())
							zf.close()
							try:
								os.remove(childzipath)
							except:
								logging.exception("when deleting child zip")
						else: # encrypt childzip and send to queue
							encsuccess, aesfilename = createAESfile(childzipath, childzipname)
							if encsuccess:
								queue.put(aesfilename)
								try:
									os.remove(childzipath)
								except:
									logging.exception("when deleting child zip")
							else:
								logging.exception("could not encrypt " + childzipath)
				if entry.is_file():
					files.append(entry.name)
		try:
			zf = zipfile.ZipFile(zipfilepath, mode='a')
		except:
			zf = zipfile.ZipFile(zipfilepath, mode='w')
		for currentfile in files:
			currentpath = os.path.join(path, currentfile)
			# correct timestamps for modification and access before 1980 to 1.1.2000
			# 1.1.1980: time.mktime(time.strptime("01 01 80", "%d %m %y")) = 315529200
			# 1.1.2000: time.mktime(time.strptime("01 01 00", "%d %m %y")) = 946681200
			if ((os.stat(currentpath).st_mtime < 315529200) or (os.stat(currentpath).st_atime < 315529200)):
				os.utime(currentpath, times = (946681200, 946681200))
			zf.write(currentpath)
		zf.close()
	except:
		logging.exception("cannot scan path or create ZIP for: " + path)
		zipfilepath = "0"  # return 0 to parent to signal the error
	return zipfilepath, zipfilename

def putFilenames(queue):
	print('putFilenames process started')
	print('module name:', __name__)
	print('parent process:', os.getppid())
	print('process id:', os.getpid())
	#!! remove old files in backupstoragepath
	for oldfile in os.listdir(backupstoragepath):
		try:
			os.remove(os.path.join(backupstoragepath, oldfile))
		except:
			logging.exception("when deleting old backup files: ")
	for backuprootpath in backuppaths:
		childzipath, childzipname = filesInDir(backuprootpath, queue)
		print("childzipath: " + childzipath)
		print("childzipname: " + childzipname)
		if childzipath != '0':
			encsuccess, aesfilename = createAESfile(childzipath, childzipname)
			if encsuccess:
				queue.put(aesfilename)
				os.remove(childzipath)
			else:
				logging.info("could not encrypt " + str(childzipath))
			try:
				os.remove(zipnamestoragefile)
			except:
				logging.exception("when deleting zip names storage file")
	queue.put("quit%&/()")

def pCloudUploadSingle(pDrive, uploadFile, uploadFolder):
	try:
		time.sleep(2)
		pDrive.uploadfile(folderid=uploadFolder, files=[uploadFile])
	except FileNotFoundError:
		logging.warning("upload single failed, file not found: " + uploadFile)
	except MemoryError:
		logging.warning("upload single failed, memory error on: " + uploadFile)
	except:
		logging.exception("upload single failed on file: " + uploadFile)
	else:
		with open(aesnamestoragefile, mode='a', encoding='utf-8') as namestorage:
			print(uploadFile, file=namestorage)
		try:
			os.remove(uploadFile)
		except:
			logging.exception("when deleting uploaded file")

def pCloudUpload(pDrive, fileBuf, bBSID, nrTries = 0):
	try:
		pDrive.uploadfile(folderid=bBSID, files=fileBuf)
	except:
		for singleFile in fileBuf:
			pCloudUploadSingle(pDrive, singleFile, bBSID)
	else:
		for actFile in fileBuf:
			with open(aesnamestoragefile, mode='a', encoding='utf-8') as namestorage:
				print(actFile, file=namestorage)
			try:
				os.remove(actFile)
			except:
				logging.exception("when deleting uploaded file")

def cleaningUpServerFiles(pDrive, bBSID):
	aesfiledates = {}
	for filename in os.listdir(scriptdatapath):
		if "aesfilenames" in filename:
			aesfiledates[filename] = filename[-12:-4] # works only for ending .txt or same length
	if len(aesfiledates) > 0:
		remoteFolderContent = pDrive.listfolder(folderid = bBSID)['metadata']['contents'] # list with all files and folders
		remoteFileNames = [filemeta['name'] for filemeta in remoteFolderContent]
		if '.DS_Store' in remoteFileNames:
			remoteFileNames.remove('.DS_Store')
		if '.localized' in remoteFileNames:
			remoteFileNames.remove('.localized')
		delist = []
		newestAESfile = sorted(aesfiledates, reverse=True)[0]
		with open(os.path.join(scriptdatapath, newestAESfile), mode='r', encoding='utf-8') as newFileNames:
			newNamesList = newFileNames.readlines()
		for remoteFile in remoteFileNames:
			delRemoteFile = True
			for newNameLine in newNamesList:
				if remoteFile in newNameLine:
					delRemoteFile = False
					break
			if delRemoteFile:
				delist.append(remoteFile)
		if len(delist) > 0:
			for oldfile in delist:
				logging.info("Obsolete old file: " + oldfile)
				# delete oldfile from pcloud server UNCOMMENT after checking logs with oldfiles
				pDrive.deletefile([filemeta['fileid'] for filemeta in remoteFolderContent if filemeta['name'] == oldfile])

def uploadFiles(queue):
	print('uploadFiles process started')
	print('module name:', __name__)
	print('parent process:', os.getppid())
	print('process id:', os.getpid())
	cloudrive = PyCloud(pclouduser, pcloudpw, endpoint = pcloudloc)
	uploadFolderID = pcloudroot # /backupAktuell/bueroserver

	uploadBuffer=[]
	keepLoop = True
	fileSum = 0
	while keepLoop:
		if (len(uploadBuffer) > 4):
			#logging.info("uploading: "  + repr(uploadBuffer))
			pCloudUpload(cloudrive, uploadBuffer, uploadFolderID)
			uploadBuffer = []
		fName = queue.get()  # blocks until new element is in queue
		#logging.info("receive queue: "  + fName)
		if (fName == "quit%&/()"):
			logging.info("total files read from queue: " + str(fileSum))
			if (len(uploadBuffer) > 0):
				pCloudUpload(cloudrive, uploadBuffer, uploadFolderID)
				uploadBuffer = []
			keepLoop = False
		else:
			infilename = re.sub(r'\n', r'', fName)
			uploadfilepath = os.path.join(backupstoragepath, infilename)
			uploadBuffer.append(uploadfilepath)
			fileSum = fileSum + 1
	logging.info(str(fileSum) + " files uploaded")
	cleaningUpServerFiles(cloudrive, uploadFolderID)

if __name__ == '__main__':
	q = Queue()
	pFproc = Process(target=putFilenames, args=(q,))
	gFproc = Process(target=uploadFiles, args=(q,))
	pFproc.start()
	gFproc.start()
	gFproc.join()
	pFproc.join()
	logging.info("\n-=-+-=-=-+-=-=-+-=-=-+-=-=-+-=-=-+-=-\ncloud backup finished\n-=-+-=-=-+-=-=-+-=-=-+-=-=-+-=-=-+-=-")
